<?php namespace Saberion\Renuka;

use Backend;
use System\Classes\PluginBase;

/**
 * Renuka Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Renuka',
            'description' => 'No description provided yet...',
            'author'      => 'Saberion',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Saberion\Renuka\Components\Home' => 'home',
            'Saberion\Renuka\Components\ProductCategory' => 'productCategory',
            'Saberion\Renuka\Components\Product' => 'product',
            'Saberion\Renuka\Components\Recipe' => 'recipe',
            'Saberion\Renuka\Components\Catalog' => 'catalog',

        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'saberion.renuka.some_permission' => [
                'tab' => 'Renuka',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'renuka' => [
                'label'       => 'Renuka',
                'url'         => Backend::url('saberion/renuka/productcategories'),
                'icon'        => 'icon-leaf',
                'permissions' => ['saberion.renuka.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'product_category' => [
                        'label' => 'Product Categories',
                        'url' => Backend::url('saberion/renuka/productcategories'),
                        'icon' => 'icon-tags',
                    ],
                    'product_sub_category' => [
                        'label' => 'Product Sub Categories',
                        'url' => Backend::url('saberion/renuka/productsubcategories'),
                        'icon' => 'icon-tags',
                    ],
                    'product' => [
                        'label' => 'Products',
                        'url' => Backend::url('saberion/renuka/products'),
                        'icon' => 'icon-cubes',
                    ],
                    'recipe_category' => [
                        'label' => 'Recipe Categories',
                        'url' => Backend::url('saberion/renuka/recipecategories'),
                        'icon' => 'icon-list',
                    ],
                    'recipe' => [
                        'label' => 'Recipes',
                        'url' => Backend::url('saberion/renuka/recipes'),
                        'icon' => 'icon-cutlery',
                    ],
                    'catalog' => [
                        'label' => 'Catalog',
                        'url' => Backend::url('saberion/renuka/catalogs'),
                        'icon' => 'icon-book',
                    ],
                ]
            ],
        ];
    }
}
