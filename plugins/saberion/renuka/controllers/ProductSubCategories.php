<?php namespace Saberion\Renuka\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Product Sub Categories Back-end Controller
 */
class ProductSubCategories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Saberion.Renuka', 'renuka', 'productsubcategories');
    }
}
