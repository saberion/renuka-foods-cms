<?php namespace Saberion\Renuka\Models;

use Model;

/**
 * Catalog Model
 */
class Catalog extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'saberion_renuka_catalogs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $rules = [
        'catalog_name' => 'required',
        'catalog_file' => 'required',

    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'catalog_file' => 'System\Models\File'
    ];
    public $attachMany = [];
}
