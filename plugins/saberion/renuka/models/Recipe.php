<?php namespace Saberion\Renuka\Models;

use Model;

/**
 * Recipe Model
 */
class Recipe extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'saberion_renuka_recipes';

    protected $slugs = ['slug' => 'recipe_name'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $jsonable = ['ingredients', 'directions'];

    protected $rules = [
        'recipe_name' => 'required',
        'recipe_description' => 'required',
        'feature_image' => 'required',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'recipe_category' => 'Saberion\Renuka\Models\RecipeCategory'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'feature_image' => 'System\Models\File',
        'video_preview_thumbnail' => 'System\Models\File'
    ];
    public $attachMany = [
        'gallery_images' => 'System\Models\File'
    ];

    public function getRecipeCategoryIdOptions(){
        return RecipeCategory::lists('recipe_category_name', 'id');
    }
}
