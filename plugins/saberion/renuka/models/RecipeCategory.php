<?php namespace Saberion\Renuka\Models;

use Model;

/**
 * RecipeCategory Model
 */
class RecipeCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'saberion_renuka_recipe_categories';

    protected $slugs = ['slug' => 'recipe_category_name'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $rules = [
        'recipe_category_name' => 'required',
        'banner_image' => 'required',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'recipes' => 'Saberion\Renuka\Models\Recipe'
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'banner_image' => 'System\Models\File'
    ];
    public $attachMany = [];
}
