<?php namespace Saberion\Renuka\Models;

use Model;

/**
 * ProductCategory Model
 */
class ProductCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'saberion_renuka_product_categories';

    protected $slugs = ['slug' => 'category_name'];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $rules = [
        'category_name' => 'required',
        'home_page_image' => 'required',
        'category_page_image' => 'required',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => [
            'Saberion\Renuka\Models\Product',
            'order' => 'sort_order asc'
            ],
        'sub_categories' => 'Saberion\Renuka\Models\ProductSubCategory'
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'home_page_image' => 'System\Models\File',
        'category_page_image' => 'System\Models\File'
    ];
    public $attachMany = [];
}
