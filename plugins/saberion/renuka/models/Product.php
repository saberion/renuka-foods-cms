<?php namespace Saberion\Renuka\Models;

use Model;

/**
 * Product Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'saberion_renuka_products';

    protected $slugs = ['slug' => 'product_name'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $rules = [
        'product_name' => 'required',
        'external_link' => 'required_if:is_external_link_available,1',
        'description' => 'required_if:is_external_link_available,0',
        'feature_image_category_page' => 'required',
        //'feature_image_home_page' => 'required',
        'gallery_images' => 'required_if:is_external_link_available,0',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product_category' => 'Saberion\Renuka\Models\ProductCategory',
        'product_sub_category' => [
            'Saberion\Renuka\Models\ProductSubCategory',
            'key' => 'sub_category_id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'feature_image_category_page' => 'System\Models\File',
        'feature_image_home_page' => 'System\Models\File'

    ];
    public $attachMany = [
        'gallery_images' => 'System\Models\File'
    ];

    public function getProductCategoryIdOptions(){
        return ProductCategory::lists('category_name', 'id');
    }

    public function getSubCategoryIdOptions(){
        $productCategoryId = $this->product_category_id;
        return ProductSubCategory::where('product_category_id', $productCategoryId)->lists('sub_category_name', 'id');
    }
}
