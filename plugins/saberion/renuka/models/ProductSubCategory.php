<?php namespace Saberion\Renuka\Models;

use Model;

/**
 * ProductSubCategory Model
 */
class ProductSubCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'saberion_renuka_product_sub_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $rules = [
        'sub_category_name' => 'required'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => [
            'Saberion\Renuka\Models\Product',
            'key' => 'sub_category_id',
            'order' => 'product_name asc'
        ]
    ];
    public $belongsTo = [
        'product_category' => 'Saberion\Renuka\Models\ProductCategory'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getProductCategoryIdOptions(){
        return ProductCategory::lists('category_name', 'id');
    }
}
