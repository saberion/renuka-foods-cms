<?php namespace Saberion\Renuka\Components;

use Cms\Classes\ComponentBase;
use Saberion\Renuka\Models\RecipeCategory;
use Input;
use Saberion\Renuka\Models\Recipe as RecipeModel;
use Illuminate\Support\Facades\URL;

class Recipe extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Recipe Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if($this->property('type') == 'recipe'){
            $this->page['recipeCategories'] = $this->recipeCategories();
            $this->page['previousUrl'] = URL::previous();
            $this->page['recipe'] = $this->recipe();
            $this->page['relatedRecipes'] = $this->relatedRecipes();
        }elseif ($this->property('type') == 'recipes'){
            $this->page['recipeCategories'] = $this->recipeCategories();
            $this->page['recipeCategory'] = $this->recipeCategory();
        }

    }

    private function recipeCategories(){
        return RecipeCategory::orderBy('recipe_category_name', 'asc')->with('recipes', 'recipes.feature_image')->paginate(10);
    }

    private function recipeCategory(){
        $recipeCategorySlug = Input::get('category');
        $recipeCategory = null;
        if($recipeCategorySlug){
            $recipeCategory = RecipeCategory::where('slug', $recipeCategorySlug)->first();
        }
        return $recipeCategory;
    }

    private function recipe(){
        $recipeSlug = $this->param('recipe_slug');
        $recipe = RecipeModel::where('slug', $recipeSlug)->first();
        return $recipe;
    }

    private function relatedRecipes(){
        $selectedRecipe = $this->recipe();

        $relatedRecipes = null;
        if($selectedRecipe) {

            $relatedRecipes = RecipeModel::where('recipe_category_id', $selectedRecipe->recipe_category_id)->where('id', '!=' , $selectedRecipe->id)->with('feature_image')->get();
        }
        return $relatedRecipes;
    }

}
