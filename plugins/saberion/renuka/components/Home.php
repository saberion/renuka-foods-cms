<?php namespace Saberion\Renuka\Components;

use Cms\Classes\ComponentBase;
use Saberion\Renuka\Models\ProductCategory;

class Home extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Home Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['productCategories'] = $this->productCategories();
    }

    private function productCategories(){
        return ProductCategory::orderBy('sort_order', 'asc')->with('products', 'home_page_image', 'products.feature_image_home_page')->get();
    }
}
