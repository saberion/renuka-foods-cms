<?php namespace Saberion\Renuka\Components;

use Cms\Classes\ComponentBase;
use Saberion\Renuka\Models\Catalog as CatalogModel;

class Catalog extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Catelog Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['catalogs'] = $this->catalogs();
    }

    private function catalogs(){
        return CatalogModel::orderBy('catalog_name', 'asc')->get();
    }
}
