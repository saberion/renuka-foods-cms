<?php namespace Saberion\Renuka\Components;

use Cms\Classes\ComponentBase;
use Saberion\Renuka\Models\ProductCategory as ProductCategoryModel;
use Input;

class ProductCategory extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ProductCategory Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['productCategories'] = $this->productCategories();
        $this->page['landingPageProductCategory'] = $this->productCategory();
    }

    private function productCategories(){
        return ProductCategoryModel::orderBy('sort_order', 'asc')->with('products', 'sub_categories')->get();
    }

    private function productCategory(){
        $categorySlug = Input::get('category');
        $productCategory = null;
        if($categorySlug){
            $productCategory = ProductCategoryModel::where('slug', $categorySlug)->first();
        }else{
            $productCategories = $this->productCategories();
            $productCategory = $productCategories->first();
        }
        return $productCategory;
    }
}
