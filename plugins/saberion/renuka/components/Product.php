<?php namespace Saberion\Renuka\Components;

use Cms\Classes\ComponentBase;
use Saberion\Renuka\Models\Product as ProductModel;
use Saberion\Renuka\Models\ProductCategory as ProductCategoryModel;
use Illuminate\Support\Facades\URL;

class Product extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Product Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['landingProduct'] = $this->product();
        $this->page['productCategories'] = $this->productCategories();
        $this->page['previousUrl'] = URL::previous();

    }

    private function product(){
        $productSlug = $this->param('product_slug');
        $product = ProductModel::where('slug', $productSlug)->first();
        return $product;
    }

    private function productCategories(){
        return ProductCategoryModel::orderBy('sort_order', 'asc')->with('products', 'sub_categories')->get();
    }
}
