<?php namespace Saberion\Renuka\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRecipeCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('saberion_renuka_recipe_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('recipe_category_name')->nullable();
            $table->string('slug')->nullable();
            //$table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('saberion_renuka_recipe_categories');
    }
}
