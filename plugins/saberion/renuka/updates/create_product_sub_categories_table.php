<?php namespace Saberion\Renuka\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductSubCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('saberion_renuka_product_sub_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('sub_category_name')->nullable();
            $table->integer('product_category_id')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('saberion_renuka_product_sub_categories');
    }
}
