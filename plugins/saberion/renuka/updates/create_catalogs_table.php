<?php namespace Saberion\Renuka\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCatalogsTable extends Migration
{
    public function up()
    {
        Schema::create('saberion_renuka_catalogs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('catalog_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('saberion_renuka_catalogs');
    }
}
