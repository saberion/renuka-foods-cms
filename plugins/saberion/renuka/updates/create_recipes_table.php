<?php namespace Saberion\Renuka\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRecipesTable extends Migration
{
    public function up()
    {
        Schema::create('saberion_renuka_recipes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('recipe_name')->nullable();
            $table->string('slug')->nullable();
            $table->integer('recipe_category_id')->unsigned();
            $table->longText('recipe_description')->nullable();
            $table->text('ingredients')->nullable();
            $table->text('directions')->nullable();
            $table->string('video_link')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('saberion_renuka_recipes');
    }
}
