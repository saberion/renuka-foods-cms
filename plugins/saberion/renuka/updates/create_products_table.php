<?php namespace Saberion\Renuka\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('saberion_renuka_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('product_name')->nullable();
            $table->string('slug')->nullable();
            $table->integer('product_category_id')->unsigned();
            $table->boolean('is_sub_category_available')->default(0);
            $table->integer('sub_category_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->string('pack_size_retail')->nullable();
            $table->string('pack_size_industry')->nullable();
            $table->string('video_link')->nullable();
            $table->boolean('is_external_link_available')->default();
            $table->string('external_link')->nullable();
            $table->integer('sort_order')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('saberion_renuka_products');
    }
}
