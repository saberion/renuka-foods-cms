window.scrollTo(0, 0);
window.onorientationchange = function() {
	window.location.reload();
};
if (!window.getComputedStyle) {
	window.getComputedStyle = function(el, pseudo) {
		this.el = el;
		this.getPropertyValue = function(prop) {
			var re = /(\-([a-z]){1})/g;

			if (re.test(prop)) {
				prop = prop.replace(re, function() {
					return arguments[2].toUpperCase();
				});
			}
			return el.currentStyle[prop] ? el.currentStyle[prop] : null;
		}
		return this;
	}
}

$(document).ready(function() {
	var winW = $(window).width();
	$(window).on('resize', function(){
		winW = $(window).width();
	});
	function isMob(){
		return winW < 768;
	}
	$('.open-popup-link').magnificPopup({
		type: 'inline',
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

    $('.popup-youtube').magnificPopup({
        disableOn: 340,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
        iframe: {
            patterns: {
                youtube: {
                    index: 'youtube.com',
                    id: 'v=',
                    src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
    });

    $(".video").fitVids();

    // $('a[href*=#]').click(function(event){
    $('html.about ul.top-menu li a').click(function(event){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        event.preventDefault();
    });

    $(".home-products-slider").each(function(index, element){
        var $this = $(this);

        var swiper = new Swiper(this, {
            slidesPerView: 2,
            spaceBetween: 20,
            pagination: false,
            nextButton: $this.parent().find(".swiper-button-next")[0],
            prevButton: $this.parent().find(".swiper-button-prev")[0],
            breakpoints: {
                1200: {
                  slidesPerView: 1,
                  spaceBetween: 20
                }
              }
        });
    });

    var swiper_product = new Swiper($(".products-slider"), {
        slidesPerView: 1,
        spaceBetween: 20,
        pagination: false,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        breakpoints: {
            1200: {
              slidesPerView: 1,
              spaceBetween: 20
            }
          }
    });

    var swiper_product_thumb = new Swiper($(".products-slider-thumb > .swiper-container"), {
        slidesPerView: 2,
        spaceBetween: 20,
        pagination: false,
    });

    $(".about-events-slider").each(function(index, element){
        var $this = $(this);

        var swiper = new Swiper(this, {
            slidesPerView: 4,
            spaceBetween: 20,
            pagination: false,
            breakpoints: {
                768: {
                  slidesPerView: 2,
                  spaceBetween: 20
                },
                480: {
                  slidesPerView: 1,
                  spaceBetween: 20
                }
              },
            nextButton: $this.parent().find(".swiper-button-next")[0],
            prevButton: $this.parent().find(".swiper-button-prev")[0]

        });
    });

    swiper_product.params.control = swiper_product_thumb;
    swiper_product_thumb.params.control = swiper_product;


    $('html.category li.has-sub-cat > a').click(function (e) {
        e.preventDefault();
        //$(this).parent('li.active').hide();
        $(this).parent('li').toggleClass('active');
        $(this).parent('li').find('ul.sub-cat-menu').slideToggle(200);

    });

});